---
layout: page
title: Privacy policy
include_in_header: false
---

# Privacy policy

<b>Introduction 
This is the Privacy Policy of apps of SpokenEnglish and its various & next versions (hereinafter collectively referred to as "us", "we", or "our"). In this Privacy Policy, we refer to our products and services as the "Service". Please read on to learn more about our data handling practices. Your use of the Service signifies that you agree with the terms of this Privacy Policy. If you do not agree with the terms of this Privacy Policy, please do not use the Service. 
​

<b>Information we collect 

We collect information from you when you use our services, including photos. We also collect information about the way our users spend time on the app. You may upload photos containing your facial data, but we will not collect your facial data.

We analyze your photos, but we do not collect facial data. After the analysis process, all data will be deleted and not saved.

The complete process is as follows:

(1) You submit the selected photo to our server in an encrypted manner;
(2) The server analyzes the photo and applies it to the chosen template;
(3) After the generation is completed, the server returns the final picture to you through the application;
(4) The final picture will be temporarily stored in encrypted storage and permanently deleted after a certain period of time. After the generation process is complete, we will immediately delete the photo you submitted to the server.

We will not share your facial data with any third party, nor will we store any facial information you submit. The photos you upload will be deleted after the analysis process is complete.

Our applications allows you to share text messages, photos, screenshots, videos and other communications in the application with other users, and if you choose to do so your text messages, photos, screenshots, videos and other communications will be stored on our servers. In addition, if you choose to share these items with other users, we may not be able to remove them from our servers or make them unavailable to anyone you have shared them with. Sending and sharing text messages, photos, screenshots, videos or other communications in the application is your decision. By choosing to share that information, you should understand that you may no longer be able to control how that information is used and that it may become publicly available (depending in part on your actions or the actions of others with whom you have shared the information). We are not responsible for any use or misuse of information you share. In addition, in case your conversation partner reports your abusive behavior or language to us, then the conversation information such as voices, screenshots or contents of conversation which have been stored only on your partner's device may be transferred to our servers. Such transferred information will be processed by us to ascertain the genuineness of the report and determine your penalty levels, if needed.

We may also collect and gather user contents (e.g., photos, screenshots, comments, and other materials) that you create on the Service. Your photo can be taken by other users on our services, and If they use capturing function provided by us, those photos can be stored and used for our services and 3rd party’s service. If you do not agree with all the terms of this Privacy Policy, please do not use the Service.To complete a commerce transaction on shop, you will be requested to provide your payment information, including your credit card number, card expiration date, CVV code, and billing address. In that event, we will transmit your information securely directly to a third party vendor or merchant who will collect such information in order to process and fulfill your purchase. We do not process or store your payment information. However, we may store other information about your purchases made on the Service, which may include the merchant’s name, the date, time and amount of the transaction and other behavioral information. 

We also collect error-reporting information if the Service crashes or hangs up so that we can investigate the error and improve the stability of the Service for future releases. In general these reports do not contain personally identifiable information, or only incidentally. As part of these error reports, we receive information about the type and version of your device, the device identifier, the time the error occurred, the feature being used and the state of the application when the error occurred. We do not use this information for any purpose other than investigating and fixing the error. 

<b>Sharing of your information 

We will not rent or sell your information to third parties without your consent, except as noted in this Privacy Policy. 
Parties with whom we may share your information: 
We may share User Content and your information (including but not limited to, information from cookies, log files, device identifiers, location data, and usage data) with businesses that are legally part of the same group of companies that is part of, or that become part of that group ("Affiliates"). Affiliates may use this information to help provide, understand, and improve the Service (including by providing analytics) and Affiliates' own services (including by providing you with better and more relevant experiences). But these Affiliates will honor the choices you make about who can see your contents. 

We also may share your information as well as information from tools like cookies, log files, and device identifiers and location data, with third-party organizations that help us provide the Service to you ("Service Providers"). Our Service Providers will be given access to your information as it is reasonably necessary to provide the Service under reasonable confidentiality terms. 
We may also share aggregate or anonymous information with third parties, including advertisers and investors. For example, we may tell our advertisers the number of users our app receives. This information does not contain any personal or personally identifiable information, and is used to develop content and services that we hope you will find of interest. 
We may remove parts of data that can identify you and share anonymized data with other parties. We may also combine your information with other information in a way that it is no longer associated with you and share that aggregated information. 

Parties with whom you may choose to share your User Content: 
Any information or content that you voluntarily disclose for posting to the Service, such as User Content, becomes available to the public. With this feature, we can be protected from exhibitionism. Once you have shared User Content or made it public, that User Content may be re-shared by others. 
If you remove information and contents (e.g. screenshots, etc.) that you created on the Service, copies may remain viewable in cached and archived pages of the Service, or if other users or third parties 、have copied or saved that information. 

<b>How we use information we collect 

Our use of this data is limited to the exclusive purpose of improving our services for our users. For example, the facial feature data we collect is vital for generating the final AI MIRROR animation. App usage data is used exclusively to better understand how users spend time in the AI MIRROR app so that we can improve the app experience.

Disclosure and Sharing

We will not share your facial data or photos with any third party, nor will we store any facial information you submit. The photos you upload will be deleted after the analysis process is complete.

AIImage does not control third-party platforms, websites, or online services, and AI MIRROR is not responsible for their actions. These platforms, websites, and online services have their own rules regarding the collection, use, and sharing of personal information. AI MIRROR encourages you to read the privacy policies of these platforms, websites, and online services to learn more about how your usage data may be used by these vendors.

<b>Security

AIImage processes your data using computers and/or other IT-enabled tools. We take technical and organizational measures to ensure that information is processed in a manner that ensures appropriate security, including protection against unauthorized or unlawful processing and against accidental loss, destruction, or damage. For example, AI MIRROR uses verified contractors who may have access to the data as specified in Section 2 of the Privacy Policy and with whom relevant data processing agreements are signed. We also provide guidance and training to our personnel to securely process your data according to our internal data protection policies.

We want to inform you that whenever you use our service, in case of an error in the app, we collect data and information (through third-party products) on your phone called Log Data. This Log Data may include information such as your device's Internet Protocol ("IP") address, device name, operating system version, the configuration of the app when utilizing our service, the time and date of your use of the service, and other statistics.

<b>Data Access and Removal 

You can always control what information you choose to share with us on the Service. To do so, you can change your settings in the Service or in your mobile device. Alternatively, you can remove the Service from your mobile device entirely.You can remove your data anytime you want. If you ask us to delete your account, we will use commercially reasonable efforts to remove your data from our servers.Any personally identifiable information that (i) you share in text messages, photos, videos or otherwise in or through the application with other users, or (ii) submit to a blog, bulletin board or chat room on our website or elsewhere, can be viewed and used by others, including to send you unsolicited messages or to commit identity theft. We are not responsible for any use or misuse of your information that might result from your disclosure of information. 

<b>Service Providers 

In certain instances, we may have contracts with third parties to provide products and/or services in connection with the Service. In those cases, we may share your personal information with our third-party service providers, but only in connection with providing the services to you. For example, we may share your phone number or email with third party SMS and email providers to deliver the authentication SMS or email to you when you register. We contractually require that these third parties use your information solely to deliver SMS or email to you and to use appropriate security measures to protect your information. In addition, certain servers are owned and hosted by third party service providers. This Privacy Policy does not apply to the practices of third party service providers. 

<b>Outbound Links 

If you accessed a website, product or service provided by a third party, including through the Service, such third party may also collect information about you. Please see the privacy policies of each such third party for more information about how they use the information they collect. This Privacy Policy does not apply to any exchange of information between you and any third party. 
Opt-Out Promotional Communications 
We allow you to choose not to receive promotional email messages and its service providers. You may opt-out by following instructions in the message sent by us or its service providers on how to unsubscribe from that particular mailing. 

<b>Security 

Protecting user privacy and personal information is a top priority for us. We make substantial efforts to ensure the privacy of all personally identifiable information you provide to us. Access to all personally identifiable information is restricted to those employees, contractors, agents and third-party service providers who need to know that information in order to provide, operate, develop, maintain, support or improve the Service. we use password protection, access logs, and system monitoring to safeguard the confidentiality and security of all member information. 
In addition, due to the inherent nature of the Internet and related technology, we do not guarantee the protection of information under our control against loss, misuse or alteration. 

<b>Age 

The Service is not directed to children under the age of thirteen (13) and we do not knowingly collect personally identifiable information from children under the age of thirteen as part of the Service. If we become aware that we have inadvertently received personally identifiable information from a user under the age of thirteen as part of the Service, we will delete such information from our records. If we change our practices in the future, we will obtain prior, verifiable parental consent before collecting any personally identifiable information from children under the age of thirteen as part of the 
Service. 

<b>Notification of Changes 

We reserve the right at our discretion to make changes to this Privacy Policy. You may review updates to our Privacy Policy at any time via links on our website. You agree to accept electronic communications and/or postings of a revised Privacy Policy on our website, and you agree that such electronic communications or postings constitute notice to you of the Privacy Policy. We reserve the right to modify this policy from time to time, so please review it frequently.If we make material changes to this policy, we will notify you by publishing a revised Privacy Policy or by means of a notice on our website, or as required by law. You agree to review the Privacy Policy periodically so that you are aware of any modifications. You agree that your continued use of the Service after we publish a revised Privacy Policy or provide a notice on our website constitutes your acceptance of the revised Privacy Policy. If you do not agree with the terms of the Privacy Policy, you should not use the Service. 

<b>Your rights 

If you wish to use any of the rights described below, you may contact us at any time by emailing us at SpokenEnglishfashion@gmail.com. We process and answer your requests without undue delay and in any event within one month of our receipt of the request unless a longer period is required due to the complexity of the request. In this case, our response time can be up to three months in total as permitted by Article 12 of the GDPR. 
1) Right to request access You have the right to request access to the data that we are processing on you, see Article 15 of the GDPR, including information about: 

the purposes of the processing the categories of personal data concerned 

the recipients or categories of recipient to whom the personal data has been or will be disclosed 

the envisaged period for which the personal data will be stored 

Furthermore, you have the right to obtain a copy of the personal data undergoing processing. Please note that access may be restricted due to intellectual property or trade secrets. 

2) The right to object You have the right to object to our processing of your personal data on grounds relating to your particular situation when the data are processed based on the balancing-of interest rule in Section 6(1)(f) of the GDPR, see Article 21 of the GDPR. In this case, we will cease the processing unless there is compelling legitimate grounds for the processing which override your interests, rights and freedoms or if the processing is necessary for the establishment, exercise or defense of legal claims. You have the right to object to our processing of your personal data for direct marketing purposes at any time. We will cease the processing of your personal data for this purpose after the objection. Please note that if you exercise this right, your user license to use the Service will cease automatically. 

3) Right to rectification and erasure You have the right to have inaccurate personal data rectified, see Article 16 of the GDPR. Furthermore, you have the right to have your personal data erased where one of the following grounds applies, see Article 17 of the GDPR: 

the personal data is no longer necessary in relation to the purposes for which they were collected or otherwise processed,
if you have withdrawn your consent and there are no other legal grounds for the processing, -if you have objected to the processing and there are no overriding legitimate grounds for the processing, 
the personal data has to be erased for compliance with a legal obligation in Union or Member State law, 
the personal data has been unlawfully processed or the personal data has been collected in relation to the offer of information society services. 
Please note that your right to erasure may be limited if the data is necessary for compliance with a legal obligation or for the establishment, exercise or defense of legal claims. 

4) Right to withdraw consentYou have the right to obtain restriction of processing in certain circumstances, see Article 18 of the GDPR. If you have the right to restriction, we will only process your data with your consent or for the establishment, exercise or defense of a legal claim or to protect a person or important grounds of public interest the right to withdraw consent. If we have asked for your consent to our processing of your data, you have the right to withdraw your consent at any time, see Article 7 of the GDPR. If you withdraw your consent, we will cease processing of the data for which you have withdrawn consent, unless we have a legal obligation to keep some or parts of your data. Please note that if you withdraw your consent, your user license to use the Services will cease automatically. The withdrawal of your consent does not affect the lawfulness of processing based on your consent before its withdrawal. 

5) The right to data portability. You have the right to receive the personal data you have provided us with which we process in a structured, commonly used and machine-readable format and have the right to transmit that data to another controller if the processing is based on consent or contract performance, see Article 20 of the GDPR. 
Contact Information 
If you have any questions about this Privacy Policy, please contact us at SpokenEnglishfashion@gmail.com. Any personally identifiable information provided in connection with inquiries related to this Privacy Policy will be used solely for the purpose of responding to the inquiry and consistent with our Privacy Policy. 

Date 
This privacy policy was posted on July 22, 2023.